﻿using SQLite;

namespace Ship.Models
{
    public class InformationItem
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int CellId { get; set; }
        public string Text { get; set; }
        public string Color { get; set; }
        public bool IsCurrent { get; set; }
    }
}
