﻿using Ship.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace Ship
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public List<InformationItem> Informations { get; set; }

        public MainPage()
        {
            InitializeComponent();
            LoadingData();
            Cell1.BindingContext = Informations[0];
            Cell2.BindingContext = Informations[1];
            Cell3.BindingContext = Informations[2];
            Cell4.BindingContext = Informations[3];
            Cell5.BindingContext = Informations[4];
            Cell6.BindingContext = Informations[5];
        }
        private void LoadingData()
        {
            Informations = App.MyDatabase.GetCurrentItems();
        }
        private void Button_Cell1(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(1);
            Cell1.BindingContext = Informations[0];
        }
        private void Button_Cell2(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(2);
            Cell2.BindingContext = Informations[1];
        }
        private void Button_Cell3(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(3);
            Cell3.BindingContext = Informations[2];
        }
        private void Button_Cell4(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(4);
            Cell4.BindingContext = Informations[3];
        }
        private void Button_Cell5(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(5);
            Cell5.BindingContext = Informations[4];
        }
        private void Button_Cell6(object sender, EventArgs e)
        {
            Informations = App.MyDatabase.EditCurrentItemByCellId(6);
            Cell6.BindingContext = Informations[5];
        }
    }
}
