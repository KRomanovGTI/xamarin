﻿using System.Collections.Generic;
using SQLite;
using Ship.Models;
using System.Linq;

namespace Ship.Data
{
    public class Database
    {
        readonly SQLiteConnection _database;

        public Database(string dbPath)
        {
            _database = new SQLiteConnection(dbPath);
            _database.CreateTable<InformationItem>();
                        
            if (_database.Table<InformationItem>().Count() == 0)
            {
                _database.Insert(new InformationItem { Id = 1, CellId = 1, Text = "green condition", Color = "Green", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 2, CellId = 1, Text = "yellow condition", Color = "Yellow", IsCurrent = false });
                _database.Insert(new InformationItem { Id = 3, CellId = 1, Text = "red condition", Color = "Red", IsCurrent = false });

                _database.Insert(new InformationItem { Id = 4, CellId = 2, Text = "sea condition", Color = "Green", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 5, CellId = 2, Text = "port condition", Color = "Red", IsCurrent = false });

                _database.Insert(new InformationItem { Id = 6, CellId = 3, Text = "stabilizers extended", Color = "Green", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 7, CellId = 3, Text = "stabilizers houses", Color = "Red", IsCurrent = false });

                _database.Insert(new InformationItem { Id = 8, CellId = 4, Text = "VLSFO", Color = "White", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 9, CellId = 4, Text = "MGO", Color = "White", IsCurrent = false });
                _database.Insert(new InformationItem { Id = 10, CellId = 4, Text = "HFO", Color = "White", IsCurrent = false });

                _database.Insert(new InformationItem { Id = 11, CellId = 5, Text = "ECA in", Color = "White", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 12, CellId = 5, Text = "ECA out", Color = "White", IsCurrent = false });

                _database.Insert(new InformationItem { Id = 13, CellId = 6, Text = "incinerators running", Color = "White", IsCurrent = true });
                _database.Insert(new InformationItem { Id = 14, CellId = 6, Text = "incinerators stopped", Color = "White", IsCurrent = false });
            }                    
        }

        public List<InformationItem> GetCurrentItems()
        {
            return _database.Table<InformationItem>().Where(i => i.IsCurrent).ToList();
        }
        public List<InformationItem> EditCurrentItemByCellId(int CellId)
        {
            InformationItem newItemCurrentFalse = new InformationItem();
            InformationItem newItemCurrentTrue = new InformationItem();

            newItemCurrentFalse = GetNewCurrentItemFalse(_database.Table<InformationItem>().Where(i => i.CellId == CellId).ToList());
            newItemCurrentTrue = GetNewCurrentItemTrue(_database.Table<InformationItem>().Where(i => i.CellId == CellId).ToList());

            _database.Update(newItemCurrentFalse);
            _database.Update(newItemCurrentTrue);

            return _database.Table<InformationItem>().Where(i => i.IsCurrent).ToList();
        }

        private InformationItem GetNewCurrentItemTrue(List<InformationItem> list)
        {
            InformationItem OldItemCurrentTrue = new InformationItem();
            InformationItem newItemCurrentTrue = new InformationItem();

            OldItemCurrentTrue = list.Where(i => i.IsCurrent).FirstOrDefault();

            if(OldItemCurrentTrue.Id == list.LastOrDefault().Id)
            {
                newItemCurrentTrue = list.FirstOrDefault();                
            }
            else
            {
                newItemCurrentTrue = list.Where(i => i.Id == OldItemCurrentTrue.Id + 1).FirstOrDefault();
            }
            newItemCurrentTrue.IsCurrent = true;
            return newItemCurrentTrue;
        }
        private InformationItem GetNewCurrentItemFalse(List<InformationItem> list)
        {
            InformationItem newItemCurrentFalse = new InformationItem();
            newItemCurrentFalse = list.Where(i => i.IsCurrent).FirstOrDefault();
            newItemCurrentFalse.IsCurrent = false;
            return newItemCurrentFalse;
        }        
    }
}